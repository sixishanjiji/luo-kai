def exp1(x,n,debug=False):
    import numpy
    if type(n)!=type(1):
        print "*** Invalid input -- n must be non-negative integer"
        return numpy.nan
    elif n<0:
        print "*** Invalid input -- n must be non-negative integer"
        return numpy.nan
    else:
        term = 1.
        partial_sum = term
        for i in range(1,n+1):
            term = term*x/i   
            partial_sum = partial_sum + term
            if debug:
                print "j = %s, term = %20.15e \n  partial_sum updated from %20.15e to %20.15e"%(i,term,partial_sum-term,partial_sum)
        return partial_sum    
    
def sin1(x,n,debug=False):
    from numpy import nan 
    if (n>=0) & (type(n)==int):     
        term = 1.
        x1 = 0.
        partial_sum = 0.
        for i in range(n+1):
            if i%2==0:
                s=0
            elif (i-1)%4==0:
                s=1
            elif (i-1)%4!=0:
                s=-1
            x1 = s*term
            partial_sum1 = partial_sum+x1
            term = term*x/(i+1)
            if debug and i>0:
                print "j=%s; term=%20.15e \n the partial sum updates from %20.15e to %20.15e" %(i,x1,partial_sum,partial_sum1)
            partial_sum = partial_sum1       
        result = partial_sum
        return result        
    else:
        print "*** Invalid input -- n must be non-negative integer"
        return nan

