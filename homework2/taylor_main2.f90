program taylor2

    implicit none                  
    real (kind=8) :: x, sine_true, y, pi
    integer :: n

    n = 20               ! number of terms to use
    pi = acos(-1.d0)
    x = 1
    sine_true = sin(x)
    call sinetaylor(x,n,y)   ! uses function below
    print *, "x = ",x
    print *, "n = ",n 
    print *, "sine_true  = ",sine_true
    print *, "sinetaylor = ",y
    print *, "error     = ",y - sine_true

end program taylor2
