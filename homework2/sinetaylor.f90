subroutine sinetaylor(x,n,f)

    implicit none
    integer, intent(in) :: n
    real(kind=8),intent(in) :: x
    real(kind=8) :: term,x1,partial_sum,partial_sum1,s
    integer :: i
    real(kind=8),intent(out) :: f
    
    term = 1
    x1 = x
    partial_sum = 0
    do i=0,n
        if(mod(i,2)==0) then
            s=0
        else if(mod(i-1,4)==0) then 
            s=1
        else if(mod(i-1,4)/=0) then 
            s=-1
        endif
        x1 = s*term
        partial_sum1 = partial_sum+x1
        term = term*x/(i+1)
        partial_sum = partial_sum1
        enddo
    f = partial_sum
    
end subroutine sinetaylor

