import matplotlib
matplotlib.use("Agg") 
from pylab import *
from taylor import sin1

figure(1)
hold(True)
x = linspace(-10,10,100)
y = sin(x)

noptions = [1,3,5,7]
szn = size(noptions)
fig = range(szn)
f = plot(x,y,label='y=sin(x)')
for i in range(szn):
    n = noptions[i]
    ysin1 = sin1(x,n)
    fig[i] = plot(x,ysin1,label='n = %s'%n)
legend(loc='upper center', bbox_to_anchor=(0.5, 1.05),
          ncol=3, fancybox=True, shadow=True)
ylim(-10,10)
xlim(-10,10)
fname = 'plot_taylor.png'
savefig(fname)
