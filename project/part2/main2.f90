program main_heat2

  
    use heat_solvers, only: solve_heat_explicit,solve_heat_implicit
    use problem, only: k, u_true
    implicit none
    integer :: n, i, nsteps, method,i1,i2,i3,i4,i5,m2,m3
    real(kind=8) :: error_max, dx, pi, tfinal, t0,dt
    real(kind=8), dimension(:), allocatable :: x, u, ustar, u0

    pi = acos(-1.d0)

    open(unit=21, file='input_data.txt', status='old')
    read(21,*) n
    read(21,*) k
    read(21,*) tfinal
    read(21,*) nsteps
    read(21,*) method
    print '("n = ",i6)', n
    print '("k = ",i6)', k
    print '("method = ",i6)', method
    print '("tfinal = ",f7.4)', tfinal
    print '("nsteps = ",i6)', nsteps

    allocate(x(0:n+1), u(0:n+1), ustar(0:n+1), u0(0:n+1))
    

    t0 = 0.d0
    dx = pi / (n+1)
    dt=tfinal/nsteps
    do i=0,n+1
        x(i) = i*dx
        end do

    do i=0,n+1
        u0(i) = sin(k*x(i))
        ustar(i) = u_true(x(i), tfinal)
        end do

    open(unit=23, file='frames.txt', status='unknown')
    do i1=0,n+1
        write(23,228) x(i1), u0(i1), ustar(i1)
228     format(3e22.14)
        end do
    
    u=u0
    if (method==1)then
    
        do m2=1,nsteps
    
        call solve_heat_explicit(x, u, t0+(nsteps-1)*dt, t0+nsteps*dt, 1, u)


        error_max = 0.d0
        do i2=0,n+1
            ustar(i2) = u_true(x(i2), tfinal)
            error_max = max(error_max, abs(u(i2) - ustar(i2)))
            end do

        print '("error_max = ", e13.6)', error_max


        do i3=0,n+1
            write(23,229) x(i3), u(i3), ustar(i3)
229         format(3e22.14)
            end do

        
        end do
        close(23)
    end if    
    
    
    if (method==2)then
    
        do m3=1,nsteps
        call solve_heat_implicit(x, u, t0+(nsteps-1)*dt,  t0+nsteps*dt, 1, u)


        error_max = 0.d0
        do i4=0,n+1
            ustar(i4) = u_true(x(i4), tfinal)
            error_max = max(error_max, abs(u(i4) - ustar(i4)))
            end do

        print '("error_max = ", e13.6)', error_max


        do i5=0,n+1
            write(23,230) x(i5), u(i5), ustar(i5)
230         format(3e22.14)
            end do
           

        end do 
        close(23)
    end if
    

end program main_heat2