
%pylab inline
import matplotlib
matplotlib.use('Agg')

from pylab import *
import JSAnimation_frametools as J

plotdir = '_plots'  # to store png files for each figure
J.make_plotdir(plotdir, clobber=True)  # ok to clobber if it already exists

input_data = loadtxt('input_data.txt')
n = int(input_data[0])
nsteps = int(input_data[1])


data = loadtxt('frames.txt')

terms = reshape(data[:,0], (n+2, nsteps+1), order='F')
fsums = reshape(data[:,1], (n+2, nsteps+1), order='F')

k=len(fsums)
x=linspace(0,1,k) 

for k in range(nterms):
    term = terms[:,k]
    fsum = fsums[:,k]



    J.save_frame(k, plotdir, verbose=True)

anim = J.make_anim(plotdir)

description = print "Heat equation with k =%4i, n=%4i, nsteps=%4i using method %4i." % (k, n, steps, method)

J.make_html(anim, file_name="heat.html", title=description)
    