
module heat_solvers

contains

subroutine solve_heat_explicit(x, u0, t0, tfinal, nsteps, u)
    implicit none
    real(kind=8), intent(in) :: x(0:), u0(0:), t0, tfinal
    integer, intent(in) :: nsteps
    real(kind=8), intent(out) :: u(0:)
    integer :: n, i, nstep
    real(kind=8) :: dx, dt, dtdx2
    real(kind=8), allocatable :: uxx(:)

    n = size(x) - 2
    dx = x(2) - x(1)
    dt = (tfinal - t0) / float(nsteps)

    print 21, n, dx, dt
21  format("Solving heat equation with n = ",i6, " dx = ",e10.3," dt = ",e10.3)

    dtdx2 = dt / dx**2
    print 22, dtdx2
22  format("  dt/dx**2 = ",f7.3)
	if (dtdx2 > 0.5d0) then
		print *, "*** Warning: the explicit method is unstable"
		endif

    allocate(uxx(1:n))

    u = u0

   u = u0

    do nstep=1,nsteps

        do i=1,n
            uxx(i) = (u(i-1) - 2.d0*u(i) + u(i+1)) / dx**2
            enddo

        do i=1,n
            u(i) = u(i) + dt * uxx(i)
            enddo

        enddo

    
    end subroutine solve_heat_explicit



subroutine solve_heat_implicit(x, u0, t0, tfinal, nsteps, u)
    implicit none
    real(kind=8), intent(in) :: x(0:), u0(0:), t0, tfinal
    integer, intent(in) :: nsteps
    real(kind=8), intent(out) :: u(0:)
    integer :: n, i, nstep,j,k,m,l
    real(kind=8) :: dx, dt, dtdx2
    real(kind=8), dimension(:),allocatable :: d1,d0,rhs,u1,d11,d01,d12
    real(kind=8), dimension(:,:),allocatable :: Im,D2,B
    integer :: INFO
    
    n = size(x) - 2
    allocate(D2(0:n-1,0:n-1),B(0:(n-1),0:(n-1)),Im(0:(n-1),0:(n-1)))
    allocate(rhs(0:n-1),d0(0:n-1),d1(0:n-1),u1(0:n-1),d11(0:n-1),d01(0:n-1),d12(0:n-1))
    
    dx = x(2) - x(1)
    dt = (tfinal - t0) / float(nsteps)
    dtdx2 = dt/dx**2
    print *, "dt / dx**2 = ", dtdx2
        
    u = u0
    
    do i=1,n-1
        D2(i,i-1)=0.5*dtdx2
        D2(i-1,i)=0.5*dtdx2
        end do
    
    do j=0,n-1
        D2(j,j)=1-dtdx2
        
        end do
        
    do k=0,n-1
        Im(k,k)=1.0
        end do

    B=d2

    
    do m=0,n-1
        u1(m)=u(m+1)
        end do
        do nstep=1,nsteps
        d1=1
        d0=-2
           
            do l=1,n-2
                
                rhs(l)=B(l,l-1)*u1(l-1)+B(l,l)*u1(l)+B(l,l+1)*u1(l+1)
                end do
                rhs(0)=B(0,0)*u1(0)+B(0,1)*u1(1)
                rhs(n-1)=B(n-1,n-2)*u1(n-2)+B(n-1,n-1)*u1(n-1)
            
            d11=-0.5*dtdx2*d1
            d12=-0.5*dtdx2*d1
            d01=d1-0.5*dtdx2*d0
            call DGTSV( n, 1,d11 ,d01, d12, rhs, n, INFO )
            u1 = rhs
            end do
        
    u(1:n)=u1
        
    end subroutine solve_heat_implicit




end module heat_solvers
