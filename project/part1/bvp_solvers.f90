
module bvp_solvers

contains

subroutine solve_bvp_direct(x, u_left, u_right, u)
    use problem, only: f
    implicit none
    real(kind=8),intent(in)  :: x(0:)
    real(kind=8),intent(in) :: u_left, u_right
    real(kind=8),dimension(:),allocatable, intent(out) :: u

    ! decleare local variables and continue writing this code...
    real(kind=8) :: dx
    integer :: n, INFO, i,j
    real (kind=8),allocatable, dimension(:) :: fx,DL,DU,D,rhs,v0,v1

    n=size(x)-2
    allocate(fx(0:n+1),DL(0:n-1),DU(0:n-1),D(0:n-1),rhs(0:n-1),u(0:n+1))
    dx=x(1)-x(0)
    do i=0,n+1
        fx(i)=f(x(i))
        enddo
    do j=0,n-1
    rhs(j) = -dx**2 * fx(j+1)
    enddo
    
    rhs(0) = rhs(0) - u_left  
    rhs(n-1) = rhs(n-1) - u_right
    
    DL=1
    DU=1
    D=-2
    call DGTSV( n, 1, DL, D, DU, rhs, n, INFO)
    
    u(0)=u_left
    u(n+1)=u_right
    
    u(1:n)=rhs
    
    print *, "Solving tridiagonal system with n =", n
 
    

    end subroutine solve_bvp_direct



subroutine solve_bvp_split(x, u_left, u_right, u)

    
    use problem, only: f
    implicit none
    real(kind=8), intent(in)  :: x(0:)
    real(kind=8), intent(in) :: u_left, u_right
    real(kind=8),dimension(:),allocatable, intent(out)  :: u
    

    ! decleare local variables and continue writing this code...
    real(kind=8) :: z,x_mid,G1,G0,dx,u_mid
    integer :: n2, INFO, i, nmid
    real (kind=8),allocatable, dimension(:) :: xhalf1,xhalf2,uhalf1,uhalf2,v0,v1

    
    n2 = size(x)
    dx = x(1)-x(0)
    nmid = int(floor(n2/2.))
    allocate(xhalf1(0:nmid),xhalf2(0:(n2-nmid)),uhalf1(0:nmid),uhalf2(0:(n2-nmid)),v0(0:n2-1),v1(0:n2-1),u(0:n2-1))
    xhalf1 = x(0:nmid)
    xhalf2 = x(nmid:n2-1)
    x_mid = x(nmid)
    

    u_mid = 0.d0
    call solve_bvp_direct(xhalf1, u_left,u_mid, uhalf1)
    call solve_bvp_direct(xhalf2, u_mid,u_right , uhalf2)
    G0 = (uhalf1(nmid-1) - 2.*u_mid + uhalf2(1))+ dx**2 * f(x_mid)
    v0(0:nmid)=uhalf1
    v0((nmid+1):n2-1)=uhalf2(1:n2-nmid)
    
    
    u_mid = 1.d0

    call solve_bvp_direct(xhalf1, u_left,u_mid, uhalf1)
    call solve_bvp_direct(xhalf2, u_mid,u_right , uhalf2)
    G1 = (uhalf1(nmid-1) - 2.*u_mid + uhalf2(1)) + dx**2 * f(x_mid)
    v1(0:nmid)=uhalf1
    v1((nmid+1):n2-1)=uhalf2(1:n2-nmid)
    
    z =  G1 / (G1 - G0)
    u = z*v0 + (1-z)*v1
    

    
    
    print *, "Computed G0 =", G0
    print *, "G1 =", G1
    print *, "z =", z


    
    end subroutine solve_bvp_split
    


subroutine solve_bvp_split_omp(x, u_left, u_right, u)
    use problem, only: f
    use omp_lib
    implicit none
    real(kind=8), intent(in)  :: x(0:)
    real(kind=8), intent(in) :: u_left, u_right
    real(kind=8),dimension(:),allocatable, intent(out)  :: u
    

    ! decleare local variables and continue writing this code...
    real(kind=8) :: z,x_mid,G1,G0,dx,u_mid
    integer :: n2, INFO, i, nmid,thread_num
    real (kind=8),allocatable, dimension(:) :: xhalf1,xhalf2,uhalf1,uhalf2,v0,v1

    
    n2 = size(x)
    dx = x(1)-x(0)
    nmid = int(floor(n2/2.))
    allocate(xhalf1(0:nmid),xhalf2(0:(n2-nmid)),uhalf1(0:nmid),uhalf2(0:(n2-nmid)),v0(0:n2-1),v1(0:n2-1),u(0:n2-1))
    xhalf1 = x(0:nmid)
    xhalf2 = x(nmid:n2-1)
    x_mid = x(nmid)
    
    ! Specify number of threads to use:
    !$ call omp_set_num_threads(4)

    print *,"nthreads =", 4
    !$omp sections PRIVATE (thread_num) ! split up work between them
        thread_num = 0
        !$omp section
        
        !$ thread_num = omp_get_thread_num() 
        u_mid = 0.
        print 211,thread_num,x(0),x(nmid),u_mid
211     format("Thread  ",i6,"taking from"  ,e10.3,"to"   ,e10.3, "with u_mid = " ,e10.3)
        call solve_bvp_direct(xhalf1, u_left,u_mid, uhalf1)
        !$omp section
        !$ thread_num = omp_get_thread_num()
        print 212,thread_num,x(nmid),x(n2-1),u_mid
212     format("Thread  ",i6,"taking from"  ,e10.3,"to"   ,e10.3, "with u_mid = " ,e10.3)
        

        call solve_bvp_direct(xhalf2, u_mid,u_right , uhalf2)
        G0 = (uhalf1(nmid-1) - 2.*u_mid + uhalf2(1))+ dx**2 * f(x_mid)
        v0(0:nmid)=uhalf1
        v0((nmid+1):n2-1)=uhalf2(1:n2-nmid)
    
        !$omp section
        !$ thread_num = omp_get_thread_num()
        u_mid = 1.
                    print 213,thread_num,x(0),x(nmid),u_mid
213    format("Thread  ",i6,"taking from"  ,e10.3,"to"   ,e10.3, "with u_mid = " ,e10.3)
        call solve_bvp_direct(xhalf1, u_left,u_mid, uhalf1)
        !$omp section
        !$ thread_num = omp_get_thread_num()
        print 214,thread_num,x(nmid),x(n2-1),u_mid
214     format("Thread  ",i6,"taking from"  ,e10.3,"to"   ,e10.3, "with u_mid = " ,e10.3)
        call solve_bvp_direct(xhalf2, u_mid,u_right , uhalf2)
        G1 = (uhalf1(nmid-1) - 2.*u_mid + uhalf2(1)) + dx**2 * f(x_mid)
        v1(0:nmid)=uhalf1
        v1((nmid+1):n2-1)=uhalf2(1:n2-nmid)
        !$OMP END SECTIONS
        
        z =  G1 / (G1 - G0)
        u = z*v0 + (1-z)*v1
    
        print *, "Computed G0 =", G0
        print *, "G1 =", G1
        print *, "z =", z

    end subroutine solve_bvp_split_omp
end module bvp_solvers
