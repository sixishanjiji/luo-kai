
module gamblers

    implicit none
    integer :: kwalks, max_steps
    save

contains

    subroutine walk(n1in, n2in, p, verbose, nsteps, winner)

    implicit none
    integer, intent(in) :: n1in,n2in
    real(kind=8), intent(in) :: p
    logical, intent(in) :: verbose
    integer, intent(out) :: nsteps, winner
    real (kind=8),allocatable, dimension(:) :: x

    ! local variables
    real(kind=8) :: r
    integer :: nstep, n1, n2
   

    ! initialize n1 and n2 with input values
    ! need to change n1 and n2 internally during this walk but do not
    ! want to affect n1 and n2 in main program. 
    n1 = n1in
    n2 = n2in
    allocate(x(max_steps))    
    call random_number(x)
   
    do nsteps=1,max_steps
        if(x(nsteps)<=p) then 
       
            n1 = n1+1
            n2 = n2-1
        else 
            n1 = n1-1
            n2 = n2+1
        endif
        if(verbose) then
            print 100, nsteps,x(nsteps),n1,n2
100         format("In step ", i6, "x = ",f12.4, "and n1 =", i6, "n2 =", i6)
        endif
        if (min(n1,n2) == 0) then
            exit
        endif
    end do
    if (verbose) then
        print 101, nsteps,n1,n2
101     format("Stopped after ", i6, "steps with n1 =", i6, "n2 =", i6)        
    endif
    if (n1 == 0) then
        winner = 2
    else if(n2 == 0) then
        winner = 1
    else
        winner = 0
    endif

    end subroutine walk

end module gamblers


