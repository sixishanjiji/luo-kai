program main2

    use random_util, only: init_random_seed
    use gamblers, only: max_steps, walk
    
    use omp_lib
    implicit none
    real(kind=8) :: p, q, p1, p2, frac1wins, frac2wins,t1, t2, elapsed_time
    integer :: seed, nthreads, k, n1, n2, nsteps, winner,points_per_thread, thread_num,istart,iend,nsteps_total,kwalks,mean_length
    integer(kind=8) :: tclock1, tclock2, clock_rate
    real (kind=8),allocatable, dimension(:) :: thread, win


    open(unit=21, file='input_data.txt', status='old')
    read(21,*) n1
    read(21,*) n2
    read(21,*) p
    read(21,*) max_steps
    read(21,*) seed
    read(21,*) kwalks
    read(21,*) nthreads

    print "('n1 = ',i6)", n1
    print "('n2 = ',i6)", n2
    print "('p = ',f8.4)", p
    print "('kwalks = ',i6)", kwalks
    print "('nthreads = ',i6)", nthreads
    print "('max_steps = ',i9)", max_steps

    allocate(win(3))
    allocate(thread(4))
   
    

    nthreads=1
    !$ nthreads=omp_get_num_threads()
    points_per_thread = (kwalks + nthreads - 1) / nthreads
    
    thread_num = 0     ! needed in serial mode

    
    istart = thread_num * points_per_thread + 1
    iend = min((thread_num+1) * points_per_thread, kwalks)
    call system_clock(tclock1)
    call cpu_time(t1)
    call init_random_seed(seed)
    !$omp parallel do private(thread_num,nsteps,winner)reduction(+:nsteps_total)
    do k=1,kwalks       
        call walk(n1, n2, p, .false., nsteps, winner)
        win(winner+1) = win(winner+1)+1
        nsteps_total =nsteps_total + nsteps
        thread_num=omp_get_thread_num()+1
        thread(thread_num)=thread(thread_num)+nsteps
        !$ thread_num = omp_get_thread_num()    ! unique for each thread
    end do
    call cpu_time(t2)   ! end cpu timer
    print 10, t2-t1
 10 format("CPU time = ",f12.8, " seconds")

    call system_clock(tclock2, clock_rate)
    elapsed_time = float(tclock2 - tclock1) / float(clock_rate)
    print 11, elapsed_time
 11 format("Elapsed time = ",f12.8, " seconds")
 
    frac1wins = win(2) / kwalks
    frac2wins = win(3) / float(kwalks)
        

        print 601, win(1),kwalks
601     format("Warning: ", f12.4," walks out of ",i8 ,"did not result in a win by either player")

        print 602, frac1wins, frac2wins
602     format("Player 1 won ",f12.4," fraction of the time, Player 2 won  ", f12.4," fraction of the time")

    q = 1-p
    P1 = (1-(q/p)**n1) / (1.-(q/p)**(n1+n2))
    P2 = 1 - P1
    print 603, P1, P2
603     format("True probabilities are P1 = ",f12.4,"  P2 =  ", f12.4)
    print 604, sum(thread)/float(kwalks)
604     format("The average path length is",f12.4)
    print 609, (n1/(q-p)) - ((n1+n2)/(q-p))*(1-(q/p)**n1)/(1-(q/p)**(n1+n2))
609     format("True mean path length is",f12.4)


    mean_length = (n1/(q-p)) - ((n1+n2)/(q-p))*(1-(q/p)**n1)/(1-(q/p)**(n1+n2))
    print 605, thread(1)
605     format("Thread  0 took  ",f12.4, "steps")
    print 606, thread(2)
606     format("Thread  1 took  ",f12.4, "steps")
    print 607, thread(3)
607     format("Thread  2 took  ",f12.4, "steps")
    print 608, thread(4)
608     format("Thread  3 took  ",f12.4, "steps")


end program main2

! I found my code here have different average path length each time I run it, which means that I get different nsteps_total.This can be caused by the omp parallel or the seed, I am not sure. But I think I have made reduction so this won't be like this.