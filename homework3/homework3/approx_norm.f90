
module approx_norm

    implicit none
    integer :: nsamples, seed, method
    save

contains

    subroutine approx_norm1(a, anorm)
    use omp_lib
    implicit none
    real(kind=8), dimension(:,:), intent(in) :: a
    real(kind=8), intent(out) :: anorm
    real (kind=8),allocatable, dimension(:) :: x
    real (kind=8),allocatable, dimension(:) :: ax
    real (kind=8), dimension(nsamples) :: x_1
    real (kind=8), dimension(nsamples) :: ax_1
    real (kind=8), dimension(nsamples) :: a_1
    integer :: i, j, n, thread_num
    
    n = size(a,1)
    allocate(x(n))
    allocate(ax(n))
    
    !$omp parallel do private(i) if(method==1)
    do j=1,nsamples
        call random_number(x)
        ax=matmul(x,a)
        !$omp parallel do if(method==2)
        do i=1,n
            x_1(j) = x_1(j)+abs(x(i))
            ax_1(j) = ax_1(j)+abs(ax(i))
            enddo
        a_1(j) = ax_1(j)/x_1(j)
        enddo
        anorm = maxval(a_1)

    
   
    
    anorm = maxval(a_1)

    end subroutine approx_norm1

end module approx_norm
